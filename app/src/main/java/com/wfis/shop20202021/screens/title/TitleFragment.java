package com.wfis.shop20202021.screens.title;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.wfis.shop20202021.R;
import com.wfis.shop20202021.databinding.TitleFragmentBinding;

import org.jetbrains.annotations.NotNull;

import timber.log.Timber;

public class TitleFragment extends Fragment {

    private NavController navController;
    private TitleFragmentBinding binding;

    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater, R.layout.title_fragment, container, false
        );

        setListeners();

        // Tell Android that our TitleFragment has a menu
        setHasOptionsMenu(true);

        // Use Timber for log
        Timber.i("onCreateView: called");
        // Do not use Log
        // Log.i("TitleFragment", "onCreateView: called");

        return binding.getRoot();
    }

    private void setListeners() {

        binding.searchView.setOnClickListener(view -> {

        });

        binding.nextFragmentButton.setOnClickListener(view -> {
                    String userName = binding.nameEditText.getText().toString();
                    navController.navigate(
                            TitleFragmentDirections.actionTitleFragmentToSecondaryFragment2(userName)
                    );
                }
        );
    }

    /* Menu methods (1), (2) */
    // (1)
    // Override onCreateOptionsMenu and inflate menu resource
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.overflow_menu, menu);
    }

    // (2)
    // Connect Menu to our NavigationUI
    // If item navigate to fragment, must have the same id as fragment in 'navigation'
    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        return NavigationUI.onNavDestinationSelected(item, navController)
                || super.onOptionsItemSelected(item);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void calledOnStart() {
        Timber.i("@OnLifecycleEvent(Lifecycle.Event.ON_START)");
    }
}
