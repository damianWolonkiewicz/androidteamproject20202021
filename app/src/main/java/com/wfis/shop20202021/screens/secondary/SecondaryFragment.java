package com.wfis.shop20202021.screens.secondary;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleObserver;

import com.wfis.shop20202021.R;
import com.wfis.shop20202021.databinding.SecondaryFragmentBinding;

import org.jetbrains.annotations.NotNull;


public class SecondaryFragment extends Fragment implements LifecycleObserver {


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SecondaryFragmentBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.secondary_fragment, container, false);

        // Receive fragment data
        SecondaryFragmentArgs args = SecondaryFragmentArgs.fromBundle(requireArguments());

        String userName = args.getUserName();
        binding.secondaryText.setText(userName);

        return binding.getRoot();
    }
}