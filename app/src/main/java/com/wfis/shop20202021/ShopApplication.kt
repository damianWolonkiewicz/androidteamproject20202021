package com.wfis.shop20202021

import android.app.Application
import timber.log.Timber

class ShopApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}